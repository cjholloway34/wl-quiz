import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { IUser } from '../interfaces/iuser.interface';
import { IApiResponse } from '../interfaces/iapi.response.interface';
import { Observable } from 'rxjs/internal/Observable';
import { User } from '../interfaces/user.model';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}
  // used for mocks
  baseUrl = 'http://localhost:3000/users/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  getUsers(): Observable<IApiResponse> {
    if (!isDevMode()) {
      return this.http.get<IApiResponse>(environment.getUsersUrl);
    } else {
      return this.http.get<IApiResponse>(this.baseUrl);
    }
  }

  getUserById(id: number): Observable<IApiResponse> {
    if (!isDevMode()) {
      return this.http.get<IApiResponse>(environment.getUserByIdUrl + id);
    } else {
      return this.http.get<IApiResponse>(this.baseUrl + id);
    }
  }

  addUser(user: User): Observable<IApiResponse> {
    if (!isDevMode()) {
      const url =
        environment.createUserUrl +
        '&id=' +
        user.id +
        '&fname=' +
        user.fname +
        '&lname=' +
        user.lname +
        '&email=' +
        user.email +
        '&accountnumber=' +
        user.accountnumber +
        '&picture=' +
        user.picture;
      return this.http.post<IApiResponse>(url, user, this.httpOptions);
    } else {
      return this.http.post<IApiResponse>(this.baseUrl, user);
    }
  }

  updateUser(user: User): Observable<IApiResponse> {
    if (!isDevMode()) {
      const url =
        environment.updateUserUrl +
        user.id +
        '&fname=' +
        user.fname +
        '&lname=' +
        user.lname +
        '&email=' +
        user.email +
        '&accountnumber=' +
        user.accountnumber +
        '&picture=' +
        user.picture;

      return this.http.post<IApiResponse>(url, user, this.httpOptions);
    } else {
      return this.http.post<IApiResponse>(this.baseUrl + user.id, user);
    }
  }

  deleteUser(id: number): Observable<IApiResponse> {
    if (!isDevMode()) {
      return this.http.delete<IApiResponse>(environment.deleteUserUrl + id);
    } else {
      return this.http.delete<IApiResponse>(this.baseUrl + id);
    }
  }
}
