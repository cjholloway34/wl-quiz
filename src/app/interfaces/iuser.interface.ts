export interface IUser {
  id: number | string;
  fname: string;
  lname: string;
  email: string;
  accountnumber: string;
  picture: string;
  createddate: string;
}
