import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { User } from 'src/app/interfaces/user.model';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.css']
})
export class AddUserModalComponent implements OnInit {
  @Input() title;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private location: Location
  ) {}

  addForm: FormGroup;
  submitted = false;

  ngOnInit() {
    const urlPattern = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    const uuidPattern =
      '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}';
    const randomPic = 'https://lorempixel.com/250/250/people/';

    this.addForm = this.formBuilder.group({
      id: ['', Validators.required],
      fname: ['', [Validators.required, Validators.minLength(2)]],
      lname: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      accountnumber: [
        '',
        [
          Validators.required,
          Validators.pattern(uuidPattern),
          Validators.minLength(36),
          Validators.maxLength(36)
        ]
      ],
      picture: ['', [Validators.required, Validators.pattern(urlPattern)]]
    });

    this.addForm.controls.accountnumber.setValue(this.uuidv4());
    this.addForm.controls.picture.setValue(randomPic);
    this.addForm.controls.id.setValue(this.generateId());
  }

  get validForm() {
    return this.addForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.apiService.addUser(this.addForm.value).subscribe(data => {
      alert('User Successfully Added!!');
      this.activeModal.close();
      this.router.navigate(['list-user']);
      this.pageRefresh();
    });
  }

  submitForm(): User {
    const formData: any = new FormData();
    formData.append('fname', this.addForm.get('fname').value);
    formData.append('lname', this.addForm.get('lname').value);
    formData.append('email', this.addForm.get('email').value);
    formData.append('accountnumber', this.addForm.get('accountnumber').value);
    formData.append('picture', this.addForm.get('picture').value);
    return formData;
  }

  generateId() {
    const a = Math.floor(Math.random() * 9999 + 999);
    let b = String(a);
    b = b.substring(0, 4);
    return b;
  }

  back() {
    this.router.navigate(['list-user']);
  }

  pageRefresh() {
    location.reload();
  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      // tslint:disable-next-line: one-variable-per-declaration
      const r = (Math.random() * 16) | 0,
        v = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }
}
